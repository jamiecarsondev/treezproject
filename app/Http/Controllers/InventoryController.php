<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Services\InventoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class InventoryController extends Controller {

    protected $inventoryRepository;
    protected $inventoryService;

    public function __construct(InventoryRepository $inventory, InventoryService $inventoryService) {
        $this->inventoryRepository = $inventory;
        $this->inventoryService = $inventoryService;
    }

    /**
     * Returns a listing of all inventory.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index() {
        return $this->inventoryRepository->getAll();
    }


    /**
     * Create a new inventory item
     *
     * @param  \Illuminate\Http\Request $request
     * @return Inventory|Response
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            throw new UnprocessableEntityHttpException();
        }

        return $this->inventoryService->createInventory($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response|int
     */
    public function show($id) {
        return $this->inventoryRepository->get($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'name' => 'nullable|max:255',
            'description' => 'nullable',
            'price' => 'nullable',
            'quantity' => 'nullable',
        ]);

        if ($validator->fails()) {
            throw new UnprocessableEntityHttpException();
        }

        return $this->inventoryService->updateInventory($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->inventoryService->delete($id);

        return response()->json([
            "message" => "records deleted"
        ], 202);
    }
}
