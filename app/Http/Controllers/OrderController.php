<?php

namespace App\Http\Controllers;

use App\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class OrderController extends Controller {

    protected $orderRepository;
    protected $orderService;

    public function __construct(OrderRepository $order, OrderService $orderService) {
        $this->orderRepository = $order;
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index() {
        return $this->orderRepository->getAll();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Order|Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:255',
            'items' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            throw new UnprocessableEntityHttpException();
        }

        return $this->orderService->createOrder($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return OrderRepository|Response
     */
    public function show($id) {
        return $this->orderRepository->getWithInventory($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'email' => 'nullable|max:255',
            'items' => 'nullable|array|min:1',
        ]);

        if ($validator->fails()) {
            throw new UnprocessableEntityHttpException();
        }

        return $this->orderService->updateOrder($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->orderService->delete($id);

        return response()->json([
            "message" => "records deleted"
        ], 202);
    }

    public function cancel($id) {
        return $this->orderService->cancelOrder($id);
    }
}
