<?php
/**
 * Created by PhpStorm.
 * User: jamiecarson
 * Date: 2019-12-29
 * Time: 7:22 PM
 */

namespace App\Http\Controllers;


use Illuminate\Foundation\Http\FormRequest;

class CreateInventoryRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];
    }
}