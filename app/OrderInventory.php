<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderInventory extends Model {

    use SoftDeletes;

    protected $table = 'order_inventory';

    protected $appends = ['inventory_name'];

    public function orders() {
        return $this->hasOne('App\Order', 'id', 'order_id');
    }

    public function inventories() {
        return $this->hasOne('App\Inventory', 'id', 'inventory_id');
    }

    public function getInventoryNameAttribute() {
        return $this->inventories->name;
    }
}
