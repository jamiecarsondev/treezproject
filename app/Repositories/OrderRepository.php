<?php
/**
 * Created by PhpStorm.
 * User: jamiecarson
 * Date: 2019-12-29
 * Time: 6:47 PM
 */

namespace App\Http\Controllers;

use App\Order;

class OrderRepository {

    public function getAll() {
        return Order::all();
    }

    public function get($id) {
        return Order::findOrFail($id);
    }

    public function getWithInventory($id) {
        return Order::with('orderInventory')->findOrFail($id);
    }

    public function delete($id) {
        return Order::destroy($id);
    }


}