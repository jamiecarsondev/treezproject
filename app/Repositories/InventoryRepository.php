<?php
/**
 * Created by PhpStorm.
 * User: jamiecarson
 * Date: 2019-12-29
 * Time: 6:47 PM
 */

namespace App\Http\Controllers;


use App\Inventory;

class InventoryRepository {

    public function getAll() {
        return Inventory::all();
    }

    public function get($id) {
        return Inventory::findOrFail($id);
    }

    public function delete($id) {
        return Inventory::destroy($id);
    }

    public function checkStock($itemId, $requestedQuantity) {
        $item = $this->get($itemId);
        return $item->quantity >= $requestedQuantity;
    }

}