<?php

namespace App\Exceptions;

use Exception;

class UpdateNotAllowedException extends Exception {
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report() {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request) {
        abort(422, 'Cannot update the quantity on an existing order. Please create a new order instead');
    }
}
