<?php

namespace App\Exceptions;

use Exception;

class NotEnoughStockException extends Exception {
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report() {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request) {
        abort(422, 'Not enough stock to complete this order');
    }
}
