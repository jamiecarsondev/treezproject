<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model {

    use SoftDeletes;

    protected $table = 'inventory';

    protected $fillable = ['name', 'description', 'price', 'quantity'];

    protected $hidden = ['deleted_at'];

    public function orders() {
        return $this->belongsToMany('App\Order', 'order_inventory');
    }

    public function orderInventory() {
        return $this->hasMany('App\OrderInventory', 'inventory_id', 'id');
    }

}
