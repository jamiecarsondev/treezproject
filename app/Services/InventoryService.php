<?php
namespace App\Services;

use App\Http\Controllers\InventoryRepository;
use App\Http\Controllers\OrderRepository;
use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: jamiecarson
 * Date: 2019-12-29
 * Time: 9:15 PM
 */
class InventoryService {

    private $inventoryRepository;
    private $orderRepository;

    /**
     * OrderService constructor.
     */
    public function __construct(InventoryRepository $inventoryRepository, OrderRepository $orderRepository) {
        $this->inventoryRepository = $inventoryRepository;
        $this->orderRepository = $orderRepository;
    }

    public function createInventory(Request $request) {
        return DB::transaction(function () use ($request) {
            $inventory = new Inventory();
            $inventory->fill($request->json()->all());
            $inventory->save();
            return $inventory;
        });
    }

    public function updateInventory(Request $request, $id) {
        return DB::transaction(function () use ($id, $request) {
            $inventory = $this->inventoryRepository->get($id);
            $inventory->fill($request->json()->all());
            $inventory->save();
            return $inventory;
        });
    }

    public function delete($id) {
        $deleted = DB::transaction(function () use ($id) {
            return $this->inventoryRepository->delete($id);
        });
        return $deleted;

    }

}