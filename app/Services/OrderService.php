<?php
namespace App\Services;

use App\Enums\OrderStatus;
use App\Exceptions\NotEnoughStockException;
use App\Exceptions\UpdateNotAllowedException;
use App\Http\Controllers\InventoryRepository;
use App\Http\Controllers\OrderRepository;
use App\Order;
use App\OrderInventory;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: jamiecarson
 * Date: 2019-12-29
 * Time: 9:15 PM
 */
class OrderService {

    private $inventoryRepository;
    private $orderRepository;

    public function __construct(InventoryRepository $inventoryRepository, OrderRepository $orderRepository) {
        $this->inventoryRepository = $inventoryRepository;
        $this->orderRepository = $orderRepository;
    }

    public function createOrder($request) {
        return DB::transaction(function () use ($request) {
            $data = $request->json()->all();

            // Create main Order object
            $order = new Order();
            $order->email = $data['email'];
            $order->order_date = now();
            $order->status = OrderStatus::New;
            $order->save();

            // Add each item to the order, checking the stock levels as we go
            foreach ($data['items'] as $item) {
                $itemId = $item['id'];
                $requestedQuantity = $item['quantity'];
                if ($this->inventoryRepository->checkStock($itemId, $requestedQuantity)) {
                    $line = new OrderInventory();
                    $line->order_id = $order->id;
                    $line->inventory_id = $itemId;
                    $line->quantity = $requestedQuantity;
                    $line->save();

                    // reduce stock of the item the customer has ordered
                    $inventory = $this->inventoryRepository->get($itemId);
                    $inventory->sell($requestedQuantity);
                    $inventory->save();
                } else {
                    throw new NotEnoughStockException();
                }

            }
            return $order;
        });
    }

    public function cancelOrder($id) {
        return DB::transaction(function () use ($id) {
            $order = $this->orderRepository->get($id);

            if (!$order->status->is(OrderStatus::Cancelled)) {
                $order->status = OrderStatus::Cancelled;
                foreach ($order->orderInventory as $item) {
                    $inventoryItem = $item->inventories;
                    $inventoryItem->quantity += $item->quantity;
                    $inventoryItem->save();
                }
            }

            $order->save();
            return $order;
        });
    }

    public function updateOrder($request, $id) {
        $data = $request->json()->all();

        // business logic to forbid adding new items to an existing order
        if (!empty($data['items'])) {
            throw new UpdateNotAllowedException();
        }

        $order = DB::transaction(function () use ($id, $data) {
            $order = $this->orderRepository->get($id);
            $order->fill($data);
            $order->save();
            return $order;
        });

        return $order;
    }

    public function delete($id) {
        $deleted = DB::transaction(function () use ($id) {
            return $this->orderRepository->delete($id);
        });
        return $deleted;
    }

}