<?php

namespace App;

use App\Enums\OrderStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {

    use SoftDeletes;
    use CastsEnums;

    protected $fillable = ['email', 'status', 'order_date'];

    protected $hidden = ['deleted_at'];

    protected $appends = ['status_readable'];

    protected $enumCasts = [
        // 'attribute_name' => Enum::class
        'status' => OrderStatus::class,
    ];

    public function inventory() {
        return $this->belongsToMany('App\Inventory', 'order_inventory');
    }

    public function orderInventory() {
        return $this->hasMany('App\OrderInventory', 'order_id', 'id');
    }

    public function getStatusReadableAttribute() {
        return $this->status->key;
    }

}
