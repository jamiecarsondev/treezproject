<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static New() - Order has been received but not processed
 * @method static static Cancelled() - Order has been cancelled
 * @method static static Dispatched() - Order has been dispatched to delivery service
 * @method static static Delivered() - Order has been delivered
 * @method static static Hold() - Order has a problem and is on hold
 */
final class OrderStatus extends Enum {
    const New = 0;
    const Cancelled = 1;
    const Dispatched = 2;
    const Delivered = 3;
    const Hold = 4;
}
