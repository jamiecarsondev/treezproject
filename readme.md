## Simple Order Service

This Simple Order Service project has been written in PHP using the Laravel framework. 

## Installation and Testing

- Laravel uses Composer to manage dependencies. Please [install Composer](https://getcomposer.org/) before starting
- Download or clone the repository into a folder of your choice
- Navigate to the top level directory and run 
<code>'composer install'</code>
- Create a new MySql database with a name of your choice, note the name and password for this database
- Create a copy of the included .env.example file and rename to .env
- Populate the new .env file with the name of your database, username and password in the DB_DATABASE, DB_USERNAME, DB_PASSWORD fields respectively
- Run
 <code>
 php artisan migrate
 </code>,
 <code>
 php artisan db:seed
 </code> and
 <code>
 php artisan serve --port=3000
 </code> to populate the database and start the serving the application
- Tests can be run from the top level directory using <code>./vendor/bin/phpunit</code>
 <bold>Please note - Tests run using a 'refresh database' trait. This will reset and reseed the database during the tests. After running tests you will need to use
  <code> php artisan migrate; php artisan db:seed</code> to use the service again. 
- A separate database can be used if setup and configured for tests using a .env.testing file. [More details can be found here](https://laravel.com/docs/5.8/testing#environment)

## Endpoints
- CRUD operation endpoints are available as specified
- A cancel order endpoint is also available at POST /orders/{id}

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.
