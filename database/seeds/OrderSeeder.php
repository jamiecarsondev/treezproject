<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('orders')->insert([
            'id' => 1,
            'email' => "order1@example.com",
            'status' => 0,
            'order_date' => Date::Now(),
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('orders')->insert([
            'id' => 2,
            'email' => "order2@example.com",
            'status' => 0,
            'order_date' => Date::Now(),
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('orders')->insert([
            'id' => 3,
            'email' => "order3@example.com",
            'status' => 0,
            'order_date' => Date::Now(),
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        // Add items to the above orders

        DB::table('order_inventory')->insert([
            'order_id' => 1,
            'inventory_id' => 1,
            'quantity' => 3,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('order_inventory')->insert([
            'order_id' => 2,
            'inventory_id' => 4,
            'quantity' => 1,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('order_inventory')->insert([
            'order_id' => 2,
            'inventory_id' => 5,
            'quantity' => 1,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('order_inventory')->insert([
            'order_id' => 3,
            'inventory_id' => 2,
            'quantity' => 2,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);
    }
}
