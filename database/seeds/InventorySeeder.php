<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class InventorySeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        echo 'seeding....';

        DB::table('inventory')->insert([
            'name' => "LA Confidential",
            'description' => "An indica-dominant strain with a strong, sharp pine aroma. Aurora's LA Confidential buds are smaller yet extremely dense and are deep green accented by a medley of red pistil hairs.",
            'price' => 30,
            'quantity' => 50,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('inventory')->insert([
            'name' => "Chocolope",
            'description' => "This indica-dominant hybrid strain has moderate levels of THC that range from 10%-16% and CBD levels of 0-1%. Dark green dried flowers feature tinges of purple and a sweet berry aroma  - this aptly named strain is bred from two popular indica strains, DJ Short Blueberry and an Afghani relative.",
            'price' => 19.97,
            'quantity' => 9,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('inventory')->insert([
            'name' => "Sour Kush",
            'description' => "Solei Sense (Sour Kush) features green buds with vibrant orange hues, that contain flavour notes of citrus. ",
            'price' => 31,
            'quantity' => 21,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('inventory')->insert([
            'name' => "Sensi Star",
            'description' => "Sensi Star stands out with crystalline trichomes on its dark green buds.  Although this high-THC indica's lineage isn't known, it is thought to be one of the strongest indicas around.",
            'price' => 47.99,
            'quantity' => 20,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);

        DB::table('inventory')->insert([
            'name' => "Shishkaberry",
            'description' => "Shishkaberry has a sweet, fruity fragrance with notes of red berries and blueberries. The flower itself is dense and covered with a mass of thick, sticky trichomes",
            'price' => 6.00,
            'quantity' => 1,
            'created_at' => Date::Now(),
            'updated_at' => Date::Now(),
        ]);
    }
}
