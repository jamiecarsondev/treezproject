<?php

namespace Tests\Unit;

use App\Enums\OrderStatus;
use App\Inventory;
use App\Order;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrderTest extends TestCase {
    use DatabaseMigrations;

    public function setUp(): void {
        parent::setUp();

        // seed some inventory into the database
        $this->seed();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllOrdersTest() {
        $response = $this->get('/orders');
        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('id', $data[0]);
        $this->assertArrayHasKey('email', $data[0]);
        $this->assertArrayHasKey('status', $data[0]);
        $this->assertArrayHasKey('status_readable', $data[0]);

        $response->assertJsonFragment([
            'email' => 'order1@example.com',
        ]);

        $response->assertJsonFragment([
            'email' => 'order2@example.com',
        ]);

        $response->assertJsonFragment([
            'email' => 'order2@example.com',
        ]);

        $response->assertJsonCount(3);


        $response->assertStatus(200);
    }

    /**
     * Test's retrieving a specific inventory item from the database
     *
     * @return void
     */
    public function testGetSingleOrdersTest() {
        $id = 2;
        $response = $this->get("/orders/$id");

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('status', $data);

        $response->assertJsonFragment([
            'email' => 'order2@example.com',
            'status' => 0,
        ]);

        $response->assertJsonCount(8);


        $response->assertStatus(200);
    }

    /**
     * Get an error when getting an id that doesn not exist
     *
     * @return void
     */
    public function testGetNotAvailableOrderTest() {
        $id = 100;
        $response = $this->get("/orders/$id");

        $response->assertStatus(404);
    }


    /**
     * Test updating an inventory item
     *
     * @return void
     */
    public function testUpdateOrder() {
        $id = 1;

        $data = [
            'status' => OrderStatus::Dispatched,
        ];

        $response = $this->json('PUT', "/orders/$id", $data);
        $response->assertStatus(200);

        $this->assertEquals(OrderStatus::getInstance(OrderStatus::Dispatched), Order::findOrFail(1)->status);
    }

    /**
     * Test updating an inventory item
     *
     * @return void
     */
    public function testUpdateQuantityOrder() {
        $id = 1;

        $data = [
            'items' => ['id' => 2, 'quantity' => 6],
        ];

        $response = $this->json('PUT', "/orders/$id", $data);
        $response->assertStatus(422);
    }

    /**
     * Test deleting an item
     *
     * @return void
     */
    public function testDeleteOrder() {
        $id = 1;
        $response = $this->delete("/orders/$id");

        $response->assertStatus(202);

        $this->assertNull(Order::find(1));
        $this->assertNotNull(Order::withTrashed()->findOrFail(1)->deleted_at);
    }


    /**
     * Test creating a new order item
     *
     * @return void
     */
    public function testCreateOrder() {
        $data = [
            "email" => "testOrder@example.com",
            "items" => [
                [
                    "id" => 1,
                    "quantity" => 3
                ],
                [
                    "id" => 2,
                    "quantity" => 1
                ]
            ]
        ];

        $response = $this->json('POST', '/orders', $data);
        $response->assertStatus(201);

        $item = Order::where('email', 'LIKE', '%testOrder@example.com%')->first();
        $this->assertNotNull($item);
        $this->assertEquals($item->email, "testOrder@example.com");

        $this->assertEquals($item->inventory[0]->id, 1);
        $this->assertEquals($item->orderInventory[0]->quantity, 3);

        $this->assertEquals($item->inventory[1]->id, 2);
        $this->assertEquals($item->orderInventory[1]->quantity, 1);

    }

    public function testCreateOrderStockRemoved() {
        $requestQuantity = 3;
        $itemId = 1;

        $data = [
            "email" => "testOrder@example.com",
            "items" => [
                [
                    "id" => $itemId,
                    "quantity" => $requestQuantity
                ]
            ]
        ];

        $stockLevelBeforeOrder = Inventory::findOrFail(1)->quantity;

        $response = $this->json('POST', '/orders', $data);
        $response->assertStatus(201);

        $item = Order::where('email', 'LIKE', '%testOrder@example.com%')->first();
        $this->assertNotNull($item);
        $this->assertEquals($item->email, "testOrder@example.com");

        $this->assertEquals($item->inventory[0]->id, $itemId);
        $this->assertEquals($item->orderInventory[0]->quantity, $requestQuantity);

        $stockLevelAfterOrder = Inventory::findOrFail($itemId)->quantity;

        $this->assertEquals($stockLevelBeforeOrder - $requestQuantity, $stockLevelAfterOrder);
    }

    /**
     * Test a failing order request
     *
     * @return void
     */
    public function testCreateOrderItemMissingData() {
        //missing items information
        $response = $this->json('POST', '/orders',
            ['email' => 'testEmail2@example.com',
            ]);

        $response
            ->assertStatus(422);

        //missing email information
        $response = $this->json('POST', '/orders',
            [
                "items" => [
                    [
                        "id" => 1,
                        "quantity" => 3
                    ],
                    [
                        "id" => 2,
                        "quantity" => 1
                    ]
                ]
            ]);

        $response
            ->assertStatus(422);
    }

    /**
     * Test cancelling a new order item
     *
     * @return void
     */
    public function testCancelOrder() {
        $id = 2;
        $order = Order::findOrFail($id);
        $orderLine1 = $order->orderInventory[0];
        $orderLine2 = $order->orderInventory[1];

        $itemsInOrder1 = $orderLine1->quantity;
        $itemsInOrder2 = $orderLine2->quantity;

        $itemsInStock1 = $orderLine1->inventories->quantity;
        $itemsInStock2 = $orderLine2->inventories->quantity;

        $this->assertEquals(OrderStatus::getInstance(OrderStatus::New), $order->status);

        $response = $this->json('POST', "/orders/cancel/$id");

        $response->assertStatus(200);

        $order = Order::findOrFail($id);
        $this->assertEquals(OrderStatus::getInstance(OrderStatus::Cancelled), $order->status);
        $orderLine1 = $order->orderInventory[0];
        $orderLine2 = $order->orderInventory[1];

        // assert new stock level is the old stock level plus the cancelled orders stock
        $this->assertEquals($orderLine1->inventories->quantity, $itemsInStock1 + $itemsInOrder1);
        $this->assertEquals($orderLine2->inventories->quantity, $itemsInStock2 + $itemsInOrder2);
    }

    /**
     * Test creating a new order item where there's not enough stock
     *
     * @return void
     */
    public function testCreateOrderNotEnoughStock() {
        $data = [
            "email" => "testOrderShouldFail@example.com",
            "items" => [
                [
                    "id" => 1,
                    "quantity" => 3000
                ]
            ]
        ];

        $response = $this->json('POST', '/orders', $data);
        $response->assertStatus(422);

        $item = Order::where('email', 'LIKE', '%testOrderShouldFail@example.com%')->first();
        $this->assertNull($item);
    }

}
