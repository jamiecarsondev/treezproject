<?php

namespace Tests\Unit;

use App\Inventory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class InventoryTest extends TestCase {

    use DatabaseMigrations;

    public function setUp(): void {
        parent::setUp();

        // seed some inventory into the database
        $this->seed();
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllInventoriesTest() {
        $response = $this->get('/inventories');
        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('id', $data[0]);
        $this->assertArrayHasKey('name', $data[0]);
        $this->assertArrayHasKey('description', $data[0]);
        $this->assertArrayHasKey('price', $data[0]);

        $response->assertJsonFragment([
            'name' => 'LA Confidential',
        ]);

        $response->assertJsonFragment([
            'name' => 'Chocolope',
        ]);

        $response->assertJsonFragment([
            'name' => 'Shishkaberry',
        ]);

        $response->assertJsonCount(5);


        $response->assertStatus(200);
    }

    /**
     * Test's retrieving a specific inventory item from the database
     *
     * @return void
     */
    public function testGetSingleInventoriesTest() {
        $id = 5;
        $response = $this->get("/inventories/$id");

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('price', $data);

        $response->assertJsonFragment([
            'name' => 'Shishkaberry',
            'price' => 6,
        ]);

        $response->assertJsonCount(7);


        $response->assertStatus(200);
    }

    /**
     * Get an error when getting an id that doesn not exist
     *
     * @return void
     */
    public function testGetNotAvailableInventoryTest() {
        $id = 100;
        $response = $this->get("/inventories/$id");

        $response->assertStatus(404);
    }


    /**
     * Test updating an inventory item
     *
     * @return void
     */
    public function testUpdateInventory() {
        $id = 1;

        $data = [
            'price' => 10,
        ];

        $response = $this->json('PUT', "/inventories/$id", $data);
        $response->assertStatus(200);

        $this->assertEquals(10, Inventory::findOrFail(1)->price);
    }

    /**
     * Test deleting an item
     *
     * @return void
     */
    public function testDeleteInventory() {
        $id = 1;
        $response = $this->delete("/inventories/$id");

        $response->assertStatus(202);

        $this->assertNull(Inventory::find(1));
        $this->assertNotNull(Inventory::withTrashed()->findOrFail(1)->deleted_at);
    }


    /**
     * Test creating a new inventory item
     *
     * @return void
     */
    public function testCreateInventoryItem() {
        $data = [
            "name" => "Hazy Kush",
            "description" => "A test item",
            "price" => 60.39,
            "quantity" => 5
        ];

        $response = $this->json('POST', '/inventories', $data);
        $response->assertStatus(201);

        $item = Inventory::where('name', 'LIKE', '%Hazy Kush%')->first();
        $this->assertNotNull($item);
        $this->assertEquals($item->name, "Hazy Kush");
        $this->assertEquals($item->description, "A test item");
        $this->assertEquals($item->price, 60.39);
        $this->assertEquals($item->quantity, 5);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testCreateInventoryItemMissingData() {
        //missing price information
        $response = $this->json('POST', '/inventories',
            ['name' => 'Test Product 1',
                'description' => 'A test product',
                'quantity' => 10,
            ]);

        $response
            ->assertStatus(422);

        //missing price information
        $response = $this->json('POST', '/inventories',
            ['name' => 'Test Product 1',
                'price' => 14,
                'quantity' => 10
            ]);

        $response
            ->assertStatus(422);

        $response = $this->json('POST', '/inventories',
            ['name' => 'Test Product 1',
                'description' => 'A test product',
                'price' => 14,
            ]);
        $response
            ->assertStatus(422);

        $response = $this->json('POST', '/inventories',
            [
                'description' => 'A test product',
                'price' => 14,
                'quantity' => 10
            ]);

        $response
            ->assertStatus(422);

    }
}
